import { Component, OnInit } from '@angular/core';
import { PasswordComponent } from "../password/password.component";
import { GrupoComponent } from "../grupo/grupo.component";
import { AuthService } from '../auth.service';
import { PasswordService } from '../password.service';
import { Password } from '../models/password';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  private passw = {
    id: "aba",
    userid: "aba",
    url: "aba",
    username: "aba",
    password: "aba",
    group: "aba"
  }
  index = 0
  public passwords: any[];
  grupos = ["All Items", "Wifi", "Social", "Gaming", "Work", "Financial"]
  constructor(public _authService: AuthService, public _passwordEvent: PasswordService, public _router:Router) { }

  ngOnInit(): void {
    this.passwords = [{
        id: "1",
        userid: "1",
        url: "www.facebook.com",
        name: "principal",
        password: "daweadxasdas",
        group: "social"
      },{
        id: "2",
        userid: "1",
        url: "www.twitter.com",
        name: "principal",
        password: "daweadxasdas",
        group: "social"
      },{
        id: "3",
        userid: "1",
        url: "www.github.com",
        name: "Aulas",
        password: "daweadxasdas",
        group: "Work"
      },{
        id: "4",
        userid: "1",
        url: "www.facebook.com",
        name: "secundaria",
        password: "daweadxasdas",
        group: "social"
      }]
    /*this._passwordEvent.getAllPasswords();
    this._passwordEvent.postPassword(this.passw);
    this.registerPassword()*/
  }

  registerPassword(){
    this._passwordEvent.postPassword(this.passw)
    .pipe(first())
      .subscribe({
        next: () => {
          console.log('Password Post was a success');
          this._router.navigate(['/dashboard'])
        },
        error: error => {
          console.log(error)
        }
      })
  }
}
