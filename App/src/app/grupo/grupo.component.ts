import { Component, OnInit } from '@angular/core';
import { PasswordService } from '../password.service';

@Component({
  selector: 'app-grupo',
  templateUrl: './grupo.component.html',
  styleUrls: ['./grupo.component.css']
})
export class GrupoComponent implements OnInit {

  groups = []
  constructor(private _passwordService: PasswordService) { }

  ngOnInit(): void {
    this.groups = [
      {
        name: "nome1",
        description: "description1"
      },
      {
        name: "nome2",
        description: "description2"
      },
      {
        name: "nome3",
        description: "description3"
      }
    ]
    /*this._eventService.getSpecialEvents()
      .subscribe(
        res => this.specialEvents = res,
        err => console.log(err)
      )*/
  }
}
