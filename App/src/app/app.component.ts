import { Component } from '@angular/core';
import { AuthService } from './auth.service';
import { User } from './models/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'PassVault';
  showFiller = false;
  user: User;
  
  constructor(public _authService: AuthService){
    this.user = this._authService.userValue;
  }
}
