import { Injectable } from '@angular/core';
import {HttpClient } from '@angular/common/http';
import { Router } from "@angular/router";
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from "./models/user";
import { map } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private userSubject: BehaviorSubject<User>;
  public user: Observable<User>;

  private baseURL = "https://localhost:5731/api/v1/users"

  private _registerUrl = this.baseURL + "/"
  private _loginUrl = this .baseURL + "/authenticate"
  
  constructor(
    private http: HttpClient,
    private _router: Router
  ) {
      this.userSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('token')));
    }

  public get userValue(): User {
    return this.userSubject.value;
  }
  
  //Create an User (Sign up)
  registerUser(user: User){
    return this.http.post(this._registerUrl, user)
  }

  //Login a User
  loginUser(user){
    return this.http.post<User>(this._loginUrl, user)
      .pipe(map(user => {
        localStorage.setItem('token',JSON.stringify(user));
        this.userSubject.next(user);
        return user;
      }))
  }

  logoutUser(){
    localStorage.removeItem('token')
    this.userSubject.next(null)
    this._router.navigate(['/home'])
  }

  getCurrentUser(){
    return localStorage.getItem('token')
  }

  loggedIn(){
    return !!localStorage.getItem('token')
  }

  getAllUsers(){
    return this.http.get<User[]>(`/rotaApi/`);
  }

  getUserById(id: string){
    return this.http.get<User>(`/rotaApi/${id}`);
  }
}
