namespace Server.Api.Dtos.Users
{
	public class UserAuthenticateResponseDto : UserReadDto
	{
		public string Token { get; set; }
	}
}