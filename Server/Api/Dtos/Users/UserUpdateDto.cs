using System;
using System.ComponentModel.DataAnnotations;

namespace Server.Api.Dtos.Users
{
	public class UserUpdateDto
	{
		[Required]
		public string Username { get; set; }
		
		[Required]
		public string Email { get; set; }

		public string FirstName { get; set; }

		public string LastName { get; set; }

		public DateTime Dob { get; set; }
	}
}