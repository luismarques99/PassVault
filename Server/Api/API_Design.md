# PassVault API Design

## Models

### User

- id: int, automatic_increment, required, unique
- email: string, required, unique
- password: string, required
- username: string, required, unique
- firstName: string
- lastName: string
- dob: string

### Account

- id: int, automatic_increment, required, unique
- userId: int, required
- url: string, required
- username: string, required
- password: string, required
- group: string

## Routes

### User

- (ADMIN) GET ("/users") - list all users
- POST ("/users") - create a new user
- GET ("/users/{userId}") - show one user by its ID
- PUT ("/users/{userId}") - update a full user by its ID
- PATCH ("/users/{userId}") - partial update a user by its ID
- (ADMIN) DELETE ("/users/{userId}") - delete a user by its ID
- POST ("/users/authenticate") - authenticate a user

### Account

- (ADMIN) GET ("/accounts") - list all accounts
- GET ("/accounts/{userId}") - list all accounts for a userId
- POST ("/accounts/{userId}") - create a new account for a userId
- GET ("/accounts/{userId}/{passwordId}") - show one account by userId and passwordId
- PUT ("/accounts/{userId}/{passwordId}") - update one account fully by userId and passwordId
- PATCH ("/accounts/{userId}/{passwordId}") - partially update one account by userId and passwordId
- DELETE ("/accounts/{userId}/{passwordId}") - delete one account by userId and passwordId