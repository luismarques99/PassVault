using System;
using System.Collections.Generic;
using System.Linq;
using Server.Api.Helpers.Exceptions;
using Server.Api.Models;
using bCrypt = BCrypt.Net.BCrypt;

namespace Server.Api.Data.Users
{
    public class SqlUsersRepository : IUsersRepository
    {
        private readonly DatabaseContext _context;

        public SqlUsersRepository(DatabaseContext context)
        {
            _context = context;
        }

        public bool SaveChanges()
        {
            return _context.SaveChanges() > 0;
        }

        public IEnumerable<User> GetAllUsers()
        {
            return _context.Users.ToList();
        }

        public User GetUserById(int id)
        {
            return _context.Users.SingleOrDefault(user => user.Id.Equals(id));
        }

        public User GetUserByUsername(string username)
        {
            return _context.Users.SingleOrDefault(other => other.Username.Equals(username));
        }

        public User GetUserByEmail(string email)
        {
            return _context.Users.SingleOrDefault(other => other.Email.Equals(email));
        }

        public void CreateUser(User user)
        {
            if (user == null) throw new ArgumentNullException(nameof(user));

            var duplicateUsername = _context.Users.SingleOrDefault(other => other.Username.Equals(user.Username));
            if (duplicateUsername != null) throw new DuplicateUserException("Username");

            var duplicateEmail = _context.Users.SingleOrDefault(other => other.Email.Equals(user.Email));
            if (duplicateEmail != null) throw new DuplicateUserException("Email");

            _context.Users.Add(user);
        }

        public void UpdateUser(User user)
        {
            // Nothing
        }

        public void DeleteUser(User user)
        {
            if (user == null) throw new ArgumentNullException(nameof(user));

            _context.Users.Remove(user);
        }

        public User AuthenticateUser(string username, string password)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password)) return null;

            var user = _context.Users.SingleOrDefault(other => other.Username.Equals(username) ||
                                                               other.Email.Equals(username));

            if (user == null) return null;

            if (!bCrypt.Verify(password, user.Password)) user = null;

            return user;
        }
    }
}