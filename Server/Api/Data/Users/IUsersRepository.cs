using System.Collections.Generic;
using Server.Api.Models;

namespace Server.Api.Data.Users
{
    public interface IUsersRepository
    {
        bool SaveChanges();
        IEnumerable<User> GetAllUsers();
        User GetUserById(int id);
        User GetUserByUsername(string username);
        User GetUserByEmail(string email);
        void CreateUser(User user);
        void UpdateUser(User user);
        void DeleteUser(User user);
        User AuthenticateUser(string username, string password);
    }
}