﻿using System.Collections.Generic;
using Server.Api.Models;

namespace Server.Api.Data.Accounts
{
    public interface IAccountsRepository
    {
        bool SaveChanges();
        IEnumerable<Account> GetAllAccounts();
        IEnumerable<Account> GetAllAccountsByUserId(int userId);
        Account GetAccountById(int id);
        void CreateAccount(Account account);
        void UpdateAccount(Account account);
        void DeleteAccount(Account account);
    }
}