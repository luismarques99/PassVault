﻿using Microsoft.EntityFrameworkCore;
using Server.Api.Models;

namespace Server.Api.Data
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
        }
        
        public DbSet<User> Users { get; set; }

        public DbSet<Account> Accounts { get; set; }

        /*protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasIndex(user => user.Email)
                .IsUnique();
            
            modelBuilder.Entity<User>()
                .HasIndex(user => user.Username)
                .IsUnique();
        }*/
    }
}