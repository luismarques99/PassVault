using AutoMapper;
using Server.Api.Dtos.Users;
using Server.Api.Models;

namespace Server.Api.Profiles
{
	public class UsersProfile : Profile
	{
		public UsersProfile()
		{
			// Source -> Target
			CreateMap<User, UserReadDto>();
			CreateMap<UserCreateDto, User>();
			CreateMap<UserUpdateDto, User>();
			CreateMap<User, UserPartialUpdateDto>();
			CreateMap<UserPartialUpdateDto, User>();
			CreateMap<User, UserAuthenticateResponseDto>();
		}
	}
}