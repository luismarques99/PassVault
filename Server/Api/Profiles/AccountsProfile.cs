﻿using AutoMapper;
using Server.Api.Dtos.Accounts;
using Server.Api.Models;

namespace Server.Api.Profiles
{
    public class AccountsProfile : Profile
    {
        public AccountsProfile()
        {
            // Source -> Target
            CreateMap<Account, AccountReadDto>();
            CreateMap<AccountCreateDto, Account>();
            CreateMap<AccountUpdateDto, Account>();
            CreateMap<Account, AccountUpdateDto>();
        }
    }
}