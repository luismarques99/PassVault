# Mockups

## Register a new user

<img src="Mockup_SignIn.png" alt="Register" width="700"/>

## User login
<img src="Mockup_LogIn.png" alt="Login" width="700"/>

## Home page
<img src="Mockup_Home.png" alt="Home" width="700"/>

## Add a new password

<img src="Mockup_NewPassword.png" alt="NewPassword" width="700"/>

## Password details
<img src="Mockup_Password.png" alt="Password" width="700"/>

## Password groups

<img src="Mockup_NewGroup.png" alt="PasswordGroups" width="700"/>
